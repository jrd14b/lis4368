# Lis4368 

## Jensen Dietz

### Lis4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/a1.md "My A1 README.md file")
    - Distributed Version Control with Git and Bitbucket
    - Java/JSP/Servlet Development Installation
    - Chapter Questions

2. [A2 README.md](a2/a2.md "My A2 README.md file")
	- Java/JSP/Servlet Development
	- Query results
	- Chapter Questions 

3. [A3 README.md](a3/a3.md "My A3 README.md file")
    - Create database solutions that interact with the Web application
    - Create a database that has 10 unique records per table
    - Database displays user entered data and allows sorting

4. [P1 README.md](p1/p1.md "My P1 README.md file")
    - Created an online portfolio
    - Project uses basic client-side validation
    - Demonstrates the use of regular expressions

5. [A4 README.md](a4/a4.md "My A4 README.md file")
    - Basic server-side validation
    - Used Model, View, Controller Framework
    - Server-side validation similar to P1

6. [A5 README.md](a5/a5.md "My A5 README.md file")
    - Basic server-side validation
    - Prepared statements to help prevent SQL injection
    - Add insert funtionality to A4

7. [P2 README.md](p2/p2.md "My P2 README.md file")
    - MVC framework
    - Basic client and server side validation 
    - JSTL to prevent XSS and CRUD functionality 



