# Lis4368

## Jensen Dietz

### Project 2 Requirements:

*Includes:*

1. MVC framework
2. Basic client and server side validation
3. JSTL to prevent XSS and CRUD functionality 

#### README.md file should include the following items:

* Valid user form entry
* Passed Validation
* Displayed and modified data 


*Online Portfolio:*
[Jensen Dietz Online Portfolio](http://localhost:9999/lis4368)

![User Form Entry](img/entry.png)

![Passed Validation](img/thanks.png)

![Display Data](img/display.png)

![Modify Form](img/modify.png)

![Delete Warning](img/delete.png)